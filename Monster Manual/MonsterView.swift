//
//  MonsterView.swift
//  Monster Manual
//
//  Created by Jeff Porter on 12/29/19.
//  Copyright © 2019 Jeff Porter. All rights reserved.
//

import SwiftUI

struct MonsterView: View {
    @State var monster: Monster
    
    var body: some View {
        VStack {
            Text(monster.name)
                .font(.headline)
            
            Text(monster.id)
                .font(.subheadline)
        }
    }
}
