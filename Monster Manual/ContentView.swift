//
//  ContentView.swift
//  Monster Manual
//
//  Created by Jeff Porter on 12/29/19.
//  Copyright © 2019 Jeff Porter. All rights reserved.
//

import SwiftUI
import Combine

struct ContentView: View {
	@ObservedObject var appState: AppState
	
    fileprivate func extractedFunc(_ monster: Monster) -> Text {
        return Text(monster.name)
    }

    /*
     
     NavigationLink(
       destination: DetailView(artwork: artwork)) {
         Text(artwork.title)
     }

     */
    var body: some View {
        NavigationView {
            List(appState.monsters) { monster in
                NavigationLink(destination: MonsterView(monster: monster)) {
                    Text(monster.name)
                }
            }
        .navigationBarTitle("Monsters")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
		ContentView(appState: AppState())
    }
}
