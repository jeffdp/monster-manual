//
//  AppState.swift
//  Monster Manual
//
//  Created by Jeff Porter on 12/29/19.
//  Copyright © 2019 Jeff Porter. All rights reserved.
//

import Foundation
import Combine

enum HTTPError: LocalizedError {
	case statusCode
	case monster
}

struct Monsters: Codable {
	let count: Int
	let results: [MonsterItem]
}

struct MonsterItem: Codable {
	let name: String
	let url: URL
}

struct Monster: Codable, Identifiable {
    enum CodingKeys: String, CodingKey {
		case id = "_id"
		case name = "name"
		case strength = "strength"
	}
	
	let id: String
	let name: String
	
	let strength: Int
}

class AppState: ObservableObject {
	@Published private(set) var monsterList: Monsters?
	@Published private(set) var monsters: [Monster] = []
	
	private var monsterTask: AnyCancellable?
	
	// TODO: This seems like a pretty naive way to handle multiple downloads. We should find a way to manage this with a max number of
	//       concurrent publishers.
	private var monsterTasks: [AnyCancellable] = []
	
	enum FailureReason : Error {
		case sessionFailed(error: URLError)
		case decodingFailed
		case other(Error)
	}

	init() {
		loadMonsters()
	}
	
	func logMonsters() {
		
		for (index, monster) in (monsterList?.results ?? []).enumerated() {
			print("\(index): \(monster.name)")
		}
	}
	
	func loadMonsters() {
		guard let url = URL(string: "http://www.dnd5eapi.co/api/monsters") else { print("bad url"); return }
		
		monsterTask = URLSession.shared.dataTaskPublisher(for: url)
		.map { $0.data }
		.decode(type: Monsters.self, decoder: JSONDecoder())
		.replaceError(with: Monsters(count: 0, results: []))
		.eraseToAnyPublisher()
		.receive(on: RunLoop.main)
		.sink(receiveValue: { monsters in
			print("received \(monsters.count)")
			self.monsterList = monsters
			
			for monster in monsters.results {
				self.loadMonster(url: monster.url)
			}
		})
	}
	
	func addMonster(monster: Monster) {
		// TODO: Find the monster first, and if it's there, replace it.
		monsters.append(monster)
	}
	
	func loadMonster(url: URL) {
		let request = makeRequest(url: url)
		let newMonsterRequest = request
		.receive(on: RunLoop.main)
		.sink { result in
			switch result {
			case .success(let monster):
				self.addMonster(monster: monster)
			case .failure(let reason):
				print("Failed: reason = \(reason)")
			}
		}
		
		monsterTasks.append(newMonsterRequest)
	}
	
	func makeRequest(url: URL) -> AnyPublisher<Result<Monster, FailureReason>, Never> {
        return URLSession.shared.dataTaskPublisher(for: url)
			.mapError { FailureReason.sessionFailed(error: $0) }
			.map { $0.data }
			.decode(type: Monster.self, decoder: JSONDecoder())
			.map { Result<Monster, FailureReason>.success($0)}
			.mapError { _ in FailureReason.decodingFailed }
			.catch { Just<Result<Monster, FailureReason>>(.failure($0)) }
			.eraseToAnyPublisher()
    }
}
